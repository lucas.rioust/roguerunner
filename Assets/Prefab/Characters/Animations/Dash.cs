﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : StateMachineBehaviour
{
    float speed = 1f;
    private float movement;
    private Vector3 startingPoint;
    private Vector3 target;
    Material hologramMat;
    
    enum DashType { x, y};
    DashType dashType;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        dashType = DashType.x;
        startingPoint = animator.transform.position;
        movement = animator.GetFloat("DashX");

        if (movement == 0f)
        {
            movement = animator.GetFloat("DashY");
            dashType = DashType.y;
        }

        hologramMat = animator.GetComponentInChildren<SkinnedMeshRenderer>().material;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        float percentageOfAnimation = stateInfo.normalizedTime;
        float newPos;
        float hologramOpacity;
        if (percentageOfAnimation < .2f)
        {
            newPos = percentageOfAnimation * 3f * movement;
            hologramOpacity = newPos / 2f;
        }
        else if (percentageOfAnimation < .5f)
        {
            newPos = (3f * .2f * movement) + (percentageOfAnimation - .2f) * movement;
            hologramOpacity = newPos / 2f;
        }
        else
        {
            newPos = (3f * .2f * movement) + .3f * movement + (percentageOfAnimation - .5f) * .2f * movement;
            hologramOpacity = newPos;
        }

        hologramMat.SetVector("Vector4_34813D08", Vector4.one * Mathf.Abs(hologramOpacity));
        if (dashType == DashType.x)
            animator.transform.position = new Vector3(startingPoint.x + newPos, startingPoint.y, startingPoint.z);
        else
            animator.transform.position = new Vector3(startingPoint.x, startingPoint.y, startingPoint.z + newPos);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (dashType == DashType.x)
            animator.transform.position = new Vector3(startingPoint.x + movement, startingPoint.y, startingPoint.z);
        else
            animator.transform.position = new Vector3(startingPoint.x, startingPoint.y, startingPoint.z + movement);

        animator.SetFloat("DashX", 0f);
        animator.SetFloat("DashY", 0f);
    }
}
