﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid
{
    private int sizeX;
    public int SizeX
    {
        get
        {
            return sizeX;
        }
    }

    private int sizeY;
    public int SizeY
    {
        get
        {
            return sizeY;
        }
    }

    public enum whatIsOnCell { player, gunner, none};

    whatIsOnCell[,] grid;

    public Grid(int x, int y)
    {
        sizeX = x;
        sizeY = y;
        grid = new whatIsOnCell[x,y];
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                grid[i, j] = whatIsOnCell.none;
            }
        }
    }

    public void AddOnCell(int x, int y, whatIsOnCell what)
    {
        grid[x,y] = what;
    }

    public whatIsOnCell[] Line(int x)
    {
        whatIsOnCell[] ret = new whatIsOnCell[sizeY];
        for (int i = 0; i < sizeY; i++){
            ret[i] = grid[x, i];
        }
        return ret;
    }

    public whatIsOnCell[] Row(int y)
    {
        whatIsOnCell[] ret = new whatIsOnCell[sizeX];
        for (int i = 0; i < sizeX; i++)
        {
            ret[i] = grid[i, y];
        }
        return ret;
    }
}
