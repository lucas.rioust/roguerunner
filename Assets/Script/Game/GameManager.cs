﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] UIManager uiManager;
    [SerializeField] EnnemiesManager ennemiesManager;
    [SerializeField] Player player;
    [SerializeField] Ennemy ennemyPrefab;

    enum State {PlayerTurn, EnnemiesEndTurn, EnnemiesTurn, PlayerEndTurn, NewEnnemies};
    enum StateStatus {Enter, Loop, Exit};
    State state = State.PlayerTurn;
    StateStatus stateStatus = StateStatus.Enter;

    private void Start()
    {
        if (!GameActionsDb.LoadGameActionsDatabase())
        {
            Application.Quit();
        }
        ennemiesManager = new EnnemiesManager(player);
    }

    void Update()
    {
        switch (state)
        {
            case State.PlayerTurn:
                PlayerTurn();
                break;
            case State.EnnemiesEndTurn:
                EnnemiesEndTurn();
                break;
            case State.EnnemiesTurn:
                EnnemiesTurn();
                break;
            case State.PlayerEndTurn:
                PlayerEndTurn();
                break;
            case State.NewEnnemies:
                NewEnnemies();
                break;
        }
    }

    void PlayerTurn()
    {
        switch (stateStatus)
        {
            case StateStatus.Enter:

                NextStateStatus();
                break;

            case StateStatus.Loop:

                if (player.nextAction != GameActionsDb.playerDb[4]) // Si le joueur a choisi une action
                {
                    player.nextAction.launch(player);
                    player.nextAction = GameActionsDb.playerDb[4];
                    if (player.checkEndTurn())
                        NextStateStatus();
                }

                break;

            case StateStatus.Exit:

                state = State.EnnemiesEndTurn;
                NextStateStatus();
                break;
        }
    }

    void EnnemiesEndTurn(){
        switch (stateStatus)
        {
            case StateStatus.Enter:

                ennemiesManager.notifyEndTurn();
                NextStateStatus();
                break;

            case StateStatus.Loop:

                NextStateStatus();  
                break;

            case StateStatus.Exit:

                state = State.EnnemiesTurn;
                NextStateStatus();
                break;
        }
    }

    void EnnemiesTurn(){
        switch (stateStatus)
        {
            case StateStatus.Enter:

                ennemiesManager.takeDecisions();
                NextStateStatus();
                break;

            case StateStatus.Loop:

                NextStateStatus();  
                break;

            case StateStatus.Exit:

                state = State.PlayerEndTurn;
                ennemiesManager.processTurn();
                NextStateStatus();
                break;
        }
    }

    void PlayerEndTurn(){
        switch (stateStatus)
        {
            case StateStatus.Enter:

                player.endTurnNotification();
                NextStateStatus();
                break;

            case StateStatus.Loop:

                NextStateStatus();  
                break;

            case StateStatus.Exit:

                state = State.NewEnnemies;
                NextStateStatus();
                break;
        }
    }

    void NewEnnemies()
    {
        switch(stateStatus)
        {
            case StateStatus.Enter:

                if (Ennemy.count < 3)
                    ennemiesManager.Spawn(ennemyPrefab);
            
                NextStateStatus();
                break;

            case StateStatus.Loop:

                NextStateStatus();
                break;

            case StateStatus.Exit:

                state = State.PlayerTurn;
                NextStateStatus();
                break;
        }
    }

    void NextStateStatus()
    {
        if (stateStatus == StateStatus.Enter)
            stateStatus = StateStatus.Loop;
        else if (stateStatus == StateStatus.Loop)
            stateStatus = StateStatus.Exit;
        else
            stateStatus = StateStatus.Enter;
    }

    public void PlayerDecision(int i)
    {
        player.nextAction = GameActionsDb.playerDb[i];
    }
}

//Logique :

//Actions de début de tour du joueur qui se concluent par une action de fin de tour.
//Actions de fin de tour précedent des ennemis 
//Actions de début de tour des ennemis qui peuvent se conclure par une action de fin de tour.
//Actions de fin de tour du joueur
//Nouveaux ennemis