﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemiesManager
{
    List<Ennemy> ennemyPool;
    Grid grid;

    public EnnemiesManager(Player player)
    {
        ennemyPool = new List<Ennemy>();
        
        grid = new Grid(9, 9);
        grid.AddOnCell(player.Position.x, player.Position.y, Grid.whatIsOnCell.player);
    }
    
    public void ProcessTurn()
    {
        notifyEndTurn();
        takeDecisions();
        processTurn();
    }


    public void notifyEndTurn()
    {
        foreach (Ennemy ennemy in ennemyPool)
        {
            ennemy.endTurnNotification();
        }
    }

    public void takeDecisions()
    {
        foreach (Ennemy ennemy in ennemyPool)
        {
            ennemy.TakeDecision();
        }
    }

    public void processTurn()
    {
        foreach (Ennemy ennemy in ennemyPool)
        {
            ennemy.nextAction.launch(ennemy);
        }
    }

    public void Spawn(Ennemy newEnemy)
    {
        Ennemy ennemy = GameObject.Instantiate(newEnemy);
        Vector2Int pos = chooseSpawnPosition();
        ennemy.transform.position = new Vector3(pos.x, .4f, pos.y);
        ennemy.gameObject.SetActive(true);
        ennemy.Position = pos;
        ennemyPool.Add(ennemy);
        grid.AddOnCell(pos.x, pos.y, Grid.whatIsOnCell.gunner);
    }
    public Vector2Int chooseSpawnPosition()
    {
        int bestRow = 8;
        int bestDepth = 8;
        for (int i = 8; i >=6; i--)
        {
            int depth = getDepthInRow(i);
            if (bestDepth > depth)
            {
                bestDepth = depth;
                bestRow = i;
            }
        }
        return new Vector2Int(8, bestRow);
    }
    public int getDepthInRow(int i)
    {
        int depth = 0;
        Grid.whatIsOnCell[] row = grid.Row(i);
        for (int j = 0; j < row.Length; j++)
        {
            if (row[j] == Grid.whatIsOnCell.gunner)
                depth = j;
        }
        return depth;
    }
    


    
}
