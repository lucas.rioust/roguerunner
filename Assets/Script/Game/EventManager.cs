﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public enum EventType { ennemySpawn }

    public EventType NewEvent()
    {
        EventType newEventType = GetRandomEnum<EventType>();
        return newEventType;
    }

    private T GetRandomEnum<T>()
    {
        System.Array A = System.Enum.GetValues(typeof(T));
        T V = (T)A.GetValue(Random.Range(0, A.Length));
        return V;
    }
}
