﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    public enum CharacterType{Player, Gunner};

    [SerializeField] private Vector2Int position;
    public Vector2Int Position
    {
        get
        {
            return position;
        }
        set
        {
            position = value;
        }
    }

    [SerializeField] public Animator animator;

    public GameAction nextAction;

    protected CharacterType characterType;
    public CharacterType Type{
        get{
            return characterType;
        }
    }

    public void move(Vector2Int move)
    {
        position.x += move.x;
        position.y += move.y;
    }

    public void setAnimationParamBool(string animationParam, bool b)
    {
        animator.SetBool(animationParam, b);
    }

    public void setAnimationParamFloat(string animationParam, float f)
    {
        animator.SetFloat(animationParam, f);
    }

    public void endTurnNotification()
    {
        animator.SetBool("TurnEnd", true);
    }
}
