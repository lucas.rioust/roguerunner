﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyAI
{
    public enum Behaviour { AimForHeroDepth, ShotWhenEnnemyOnLine}

    List<Behaviour> BehaviourPriorityList;
    Player player;
    Ennemy ennemy;
    
    public EnnemyAI(List<Behaviour> behaviourPriorityList, Player player, Ennemy ennemy)
    {
        BehaviourPriorityList = new List<Behaviour>(behaviourPriorityList);
        this.player = player;
        this.ennemy = ennemy;
    }

    
    private enum ChangeSpeed { decelerate = -1, no = 0, accelerate = 1};
    private enum ShouldShotLine { left = -1, no = 0, right = 1};

    public GameAction choseAction(List<GameAction> actionPool)
    {
        foreach(Behaviour b in BehaviourPriorityList)
        {
            actionPool = ChooseBestCandidate(b, actionPool);
        }

        int r = Random.Range(0, actionPool.Count);
        return actionPool[r];
    }
    private List<GameAction> ChooseBestCandidate(Behaviour b, List<GameAction> pool)
    {
        List<GameAction> bestCandidates = new List<GameAction>();
        ChangeSpeed shouldDashRow = ChangeSpeed.no;
        ShouldShotLine shouldShot = ShouldShotLine.no;

        switch (b)
        {
            case Behaviour.AimForHeroDepth:

                if (player.Position.x > ennemy.Position.x)
                    shouldDashRow = ChangeSpeed.accelerate;
                else if (player.Position.x < ennemy.Position.x)
                    shouldDashRow = ChangeSpeed.decelerate;
                break;


            case Behaviour.ShotWhenEnnemyOnLine:

                int rollDice = Random.Range(0, 100);
                if ((ennemy.Position.x == player.Position.x) || (rollDice > 50 && Mathf.Abs(ennemy.Position.x - player.Position.x) == 1))
                {
                    if (ennemy.Position.y > player.Position.y)
                        shouldShot = ShouldShotLine.right;
                    else
                        shouldShot = ShouldShotLine.left;
                }
                break;
        }


        foreach (GameAction gameAction in pool)
        {
            if (!gameAction.hasTag(ActionTag.ShotLine, (int)shouldShot))
                continue;

            else if (!gameAction.hasTag(ActionTag.DashX, (int)shouldDashRow))
                continue;

            bestCandidates.Add(gameAction);
        }
        if (bestCandidates.Count != 0)
            return bestCandidates;
        else
            return pool;
    }
}
