﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gunner : Ennemy
{

    protected override void Awake()
    {
        base.Awake();
        List<EnnemyAI.Behaviour> behaviourList = new List<EnnemyAI.Behaviour>() {EnnemyAI.Behaviour.ShotWhenEnnemyOnLine, EnnemyAI.Behaviour.AimForHeroDepth };
        ai = new EnnemyAI(behaviourList, player, this);
        characterType = CharacterType.Gunner;
    }

    public void checkAnimationChange(GameAction gameAction)
    {
        if (animator.GetBool("AimRight"))
        {
            animator.SetBool("AimRight", false);
            animator.SetTrigger("ShotRight");
        }

        if (animator.GetBool("AimLeft"))
        {
            animator.SetBool("AimLeft", false);
            animator.SetTrigger("ShotLeft");
        }
    }
}
