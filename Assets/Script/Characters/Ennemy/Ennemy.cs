﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ennemy : Character
{
    protected virtual void Awake()
    {
        count++;
    }
    [SerializeField] protected Player player;
    
    public static int count = 0;
    protected EnnemyAI ai;

    public void TakeDecision()
    {
        nextAction = ai.choseAction(GameActionsDb.gunnerDb);
    }

}
