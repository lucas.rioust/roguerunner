﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameAction
{
    string name;
    public string Name
    {
        get
        {
            return name;
        }
    }

    List<(Func<int, int>, int)> funcs;

    List<(ActionTag, int)> tags;
    public bool hasTag(ActionTag actionTag, int i)
    {
        foreach((ActionTag, int) tag in tags)
        {
            if (tag.Item2 == 0 && tag.Item1 == actionTag)
                return false;
            if (tag.Item1 == actionTag && tag.Item2 == i)
                return true;
        }
        if (i == 0)
            return true;
        return false;
    }
    ActionClassRestriction restriction;
    public ActionClassRestriction Restriction
    {
        get
        {
            return restriction;
        }
    }

    Character c;


    //Constructor
    public GameAction(string name, List<(ActionTag, int)> tags, ActionClassRestriction restriction)
    {
        this.name = name;
        this.tags = new List<(ActionTag, int)>(tags);
        this.restriction = restriction;
        makeFunctions();
    }



    //Launch action
    public void launch(Character c)
    {
        this.c = c;
        foreach ((Func<int, int>, int) func in funcs)
        {
            func.Item1(func.Item2);
        }
        if (c.Type == Character.CharacterType.Player)
            ((Player)c).didAction();
    }



    // Functions

    private int Shot(int i)
    {
        if (i == 1)
            c.setAnimationParamBool("AimRight", true);
        else
            c.setAnimationParamBool("AimLeft", true);
        return 0;
    }

    private int DashX(int i)
    {
        c.move(new Vector2Int(i, 0));
        c.setAnimationParamFloat("DashX", (float)i);
        return 0;
    }

    private int DashY(int i)
    {
        c.move(new Vector2Int(0, i));
        c.setAnimationParamFloat("DashY", (float)i);
        return 0;
    }

    //Assemble action's functions
    private void makeFunctions()
    {
        funcs = new List<(Func<int, int>, int)>();
        foreach((ActionTag, int) tag in tags)
        {
            if (tag.Item1 == ActionTag.DashX)
                funcs.Add((DashX, tag.Item2));

            if (tag.Item1 == ActionTag.DashY)
                funcs.Add((DashY, tag.Item2));

            if (tag.Item1 == ActionTag.ShotLine)
                funcs.Add((Shot, tag.Item2));
        }
    }
    
}
