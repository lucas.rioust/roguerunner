﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    int actionsPerTurn = 1; //Temporaire
    int actionsThisTurn = 0; //Temporaire

    public void Start() {
           nextAction = GameActionsDb.playerDb[4];
           characterType = CharacterType.Player;
    }

    public bool checkEndTurn(){
        if (actionsPerTurn != actionsThisTurn)
            return false;
        actionsThisTurn = 0;
        return true;
    }

    public void didAction(){
        actionsThisTurn++;
    }

}
