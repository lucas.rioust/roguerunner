﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public enum ActionTag { DashX, DashY, ShotLine};
public enum ActionClassRestriction { Common, Ennemy, Player, Gunner };

public static class GameActionsDb
{
    public static List<GameAction> db;
    public static List<GameAction> playerDb;
    public static List<GameAction> gunnerDb;

    public static bool LoadGameActionsDatabase()
    {
        db = new List<GameAction>();
        parseXml();
        feedPlayerDb();
        feedGunnerDb();
        return true;
    }

    public static GameAction Find(List<GameAction> list, string actionName)
    {
        foreach (GameAction action in list)
        {
            if (action.Name == actionName)
                return action;
        }
        return null;
    }

    public static GameAction RandomAction(List<GameAction> list)
    {
        int r = UnityEngine.Random.Range(0, list.Count);
        return list[r];
    }

    private static void feedPlayerDb()
    {
        playerDb = new List<GameAction>();
        foreach(GameAction gameAction in db)
        {
            if (gameAction.Restriction == ActionClassRestriction.Common ||
                gameAction.Restriction == ActionClassRestriction.Player)
            {
                playerDb.Add(gameAction);
            }
        }
    }
    private static void feedGunnerDb()
    {
        gunnerDb = new List<GameAction>();
        foreach (GameAction gameAction in db)
        {
            if (gameAction.Restriction == ActionClassRestriction.Common ||
                gameAction.Restriction == ActionClassRestriction.Ennemy ||
                gameAction.Restriction == ActionClassRestriction.Gunner)
            {
                gunnerDb.Add(gameAction);
            }
        }
    }

    private static void parseXml()
    {
        XmlDocument doc = new XmlDocument();
        doc.Load("Assets/Script/Database/GameActionsDb.xml");

        XmlNodeList GameActions = doc.SelectSingleNode("GameActions").SelectNodes("GameAction");
        foreach (XmlNode GameAction in GameActions)
        {
            string name;
            List<(ActionTag, int)> tags = new List<(ActionTag, int)>();
            ActionClassRestriction restriction;

            name = GameAction.SelectSingleNode("Name").InnerText;
            Enum.TryParse(GameAction.SelectSingleNode("ActionClassRestriction").InnerText, out restriction);

            foreach (XmlNode ActionTag in GameAction.SelectSingleNode("ActionTags").SelectNodes("ActionTag"))
            {
                ActionTag tag;
                Enum.TryParse(ActionTag.SelectSingleNode("Name").InnerText, out tag);
                int param = Int32.Parse(ActionTag.SelectSingleNode("Param").InnerText);
                tags.Add((tag, param));
            }
            db.Add(new GameAction(name, tags, restriction));
        }
    }
}
