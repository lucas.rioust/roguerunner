﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] int startingGenerationNumber;
    [SerializeField] GameObject Terrain;
    [SerializeField] float speed;
    int currentTick = 0;
    Vector3 direction;
    private void Start()
    {
        for (int i = 0; i< startingGenerationNumber; i++)
        {
            generateNewGround();
        }

        direction = speed * Vector3.left;
    }

    void Update()
    {
        transform.Translate(direction * Time.deltaTime);
    }


    public void generateNewGround()
    {
        GameObject newGround = GameObject.Instantiate(Terrain, transform);
        newGround.transform.localPosition = new Vector3(currentTick, 0, 0);
        newGround.SetActive(true);
        currentTick++;
    }
}
